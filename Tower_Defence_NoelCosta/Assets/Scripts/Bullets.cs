﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets : MonoBehaviour
{
    private float speed = 50f;

    private Rigidbody2D rigidBodyVar;

    // Start is called before the first frame update
    void Start()
    {
        rigidBodyVar = GetComponent<Rigidbody2D>(); //Saves RigidBody2D component to a variable
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rigidBodyVar.AddForce(transform.right * speed); //Adds force to bullet object to make it move
    }

    void OnTriggerEnter2D(Collider2D hitObject)
    {
        /* If the bullet hits a destroy collider, the bullet will be destroyed */
        if (hitObject.gameObject.tag == "destroy_collider")
        {
            Destroy(this.gameObject);
        }

        /* Level Missile bullet 
        if (hitObject.gameObject.tag.Contains("enemy_"))
        {
            Turrets selectedTurret_script = levelManager.selectedTurret.GetComponent(typeof(Turrets)) as Turrets;

            Enemy hitObject = Enemy.fingenemy_health -= 50;
            checkEnemyHealth();
        } 
        
        spawn damage on spot
         
         */
    }
}

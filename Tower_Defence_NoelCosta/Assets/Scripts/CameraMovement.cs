﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    /* Used to move camera with middle mouse button */
    private Vector3 mouseStartPos;
    private Vector3 mouseMoveToPos;

    /* Used for Zooming with middle mouse */
    private float defaultZAxis;
    private float currentZAxis;
    private float zoomSensitivity = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        defaultZAxis = Camera.main.orthographicSize;
        currentZAxis = defaultZAxis;
    }

    // Update is called once per frame
    void Update()
    {
        //Mouse Movement - x / y axis
        if (Input.GetMouseButtonDown(2)) { //Gets Starting Position
            mouseStartPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, currentZAxis);
            mouseStartPos = Camera.main.ScreenToWorldPoint(mouseStartPos);
        }

        else if (Input.GetMouseButton(2)){ //To get the position to move to
            mouseMoveToPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, currentZAxis);
            mouseMoveToPos = Camera.main.ScreenToWorldPoint(mouseMoveToPos);

            transform.position = transform.position - (mouseMoveToPos - mouseStartPos);
        }

        //Mouse Scroll - z axis
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && currentZAxis > defaultZAxis / 2)
        {
            currentZAxis -= zoomSensitivity; //Zoom Out
            GetComponent<Camera>().orthographicSize = currentZAxis;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0 && currentZAxis < (defaultZAxis * 1.5f))
        {
            currentZAxis += zoomSensitivity; //Zoom In
            GetComponent<Camera>().orthographicSize = currentZAxis;
        }
    }
}

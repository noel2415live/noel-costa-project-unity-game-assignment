﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    /* Creates LevelManager game object */
    public LevelManager levelManager;

    /* Enemy Movement Variables */
    private int nextPointNum = 0;
    private Vector3 nextEnemyPos;
    private static float moveSpeed = 3f;
    private static float rotateSpeed = 9f;

    /* Enemy Health */
    public int health_hellhound = 50;
    public int health_zombie = 150;
    public int health_boss = 500;

    public int enemy_health;

    private ParticleSystem bloodParticle;

    private void Awake()
    {
        levelManager = GameObject.FindObjectOfType<LevelManager>(); //Saves LevelManager game object to a variable

        bloodParticle = Resources.Load("Prefabs/Particles/bloodParticles", typeof(ParticleSystem)) as ParticleSystem;
    }

    // Start is called before the first frame update
    void Start()
    {
        /* Adds health accordingly to the type of Enemy */
        if (this.CompareTag("enemy_hellhound"))
        {
            enemy_health = health_hellhound;
        }
        else if (this.CompareTag("enemy_zombie"))
        {
            enemy_health = health_zombie;
        }
        else if (this.CompareTag("enemy_boss"))
        {
            enemy_health = health_boss;
        }

        /* Sets Starting Position For Enemies */
        transform.position = new Vector3(levelManager.allPointsList[0].x, levelManager.allPointsList[0].y, transform.position.z);
    }

    private void Update()
    {
        /* Starts moving the enemies on their path */
        Move();
    }

    /* Starts moving the enemies on their path */
    void Move() {
        /* Creates a vector 3 Variable to store */
        Vector3 moveTo = new Vector3(levelManager.allPointsList[nextPointNum].x, levelManager.allPointsList[nextPointNum].y, transform.position.z);

        /* Sets Rotating Angle */
        Vector3 rotateTo = moveTo - transform.position;
        float rotateToAngle = Mathf.Atan2(rotateTo.y, rotateTo.x) * Mathf.Rad2Deg;
        
        /* Rotates to the angle */
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(rotateToAngle, Vector3.forward), rotateSpeed * Time.deltaTime);

        /* Moves it to the new location */
        transform.position = Vector3.MoveTowards(transform.position, moveTo, moveSpeed * Time.deltaTime);

        /* Stores current position of the enemy */
        Vector3 currentPos = transform.position;

        /* If enemy moved to the next point on the path then the point number variable will add up to the next one */
        if (currentPos == moveTo)
            {
                nextPointNum++;
            }

        /* When the enemy reaches the end of the path, the enemy game object will be destroyed */
        if (nextPointNum == levelManager.allPointsList.Count) {
            Destroy(this.gameObject);
            levelManager.baseHealth -= 1;
        }
        
    }

    /* Bullet Damage to remove from Enemy Health */
    void OnTriggerEnter2D(Collider2D hitObject)
    {
        Instantiate(bloodParticle, this.transform.position, this.transform.rotation);
        /* Level 1 bullet */
        if (hitObject.gameObject.tag == "bullet_lvl_01")
        {
            enemy_health -= 50;
            checkEnemyHealth();
        }
        /* Level 2 bullet */
        if (hitObject.gameObject.tag == "bullet_lvl_02")
        {
            enemy_health -= 250;
            checkEnemyHealth();
        }
        /* Level 3 bullets */
        if ((hitObject.gameObject.tag == "bullet_lvl_03_blue") || (hitObject.gameObject.tag == "bullet_lvl_03_green") || (hitObject.gameObject.tag == "bullet_lvl_03_purple") || (hitObject.gameObject.tag == "bullet_lvl_03_red"))
        {
            enemy_health -= 1250;
            checkEnemyHealth();
        }
    }

    void checkEnemyHealth()
    {
        /* Destroys enemy game object if health is 0 or below */
        if (enemy_health <= 0)
        {
            Destroy(this.gameObject);

            levelManager.kills++; //kill is added to the player stats
            levelManager.money += 60;
        }
        else
        {
            levelManager.money += 10;
        }
    }

}
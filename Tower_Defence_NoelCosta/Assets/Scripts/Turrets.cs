﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turrets : MonoBehaviour
{
    /* Creates LevelManager game object */
    private LevelManager levelManager;

    /* Holds position of the closest enemy */
    public Vector2 closestEnemy;

    /* States whether an enemy has been found */
    public bool enemyFound;

    /* Game objects store bullet prefabs */
    public GameObject bullet_lvl_1;
    public GameObject bullet_lvl_2;
    public GameObject bullet_lvl_3_blue;
    public GameObject bullet_lvl_3_green;
    public GameObject bullet_lvl_3_purple;
    public GameObject bullet_lvl_3_red;
    public GameObject missile_lvl_1;
    public GameObject missile_lvl_2;
    public GameObject missile_lvl_3;

    /* Bullet Sound Effects */

    public AudioClip bullet_lvl_1_shot;
    public AudioClip bullet_lvl_2_shot;
    public AudioClip bullet_lvl_3_shot;

    public AudioSource audio_source;

    /* Game objects store the required barrels for turret type */
    public GameObject single_barrel;
    public GameObject double_barrel_1;
    public GameObject double_barrel_2;
    public GameObject triple_barrel_1;
    public GameObject triple_barrel_2;
    public GameObject triple_barrel_3;
    public GameObject sniper_barrel;
    public GameObject missile_spot;

    /* Turret top and base  */
    public GameObject turret_top;
    public GameObject turret_base;
    public GameObject turret_safeArea;
    
    private bool clicked_thisTurret;

    /* Turret Properties */
    public string turret_name;

    string turret_colour;
    string turret_type;

    public byte turret_colour_level;
    public byte turret_fireRate_level;

    public int turret_range;
    public float turret_fireRate;

    public float timeSinceLastShot = 0;
    public bool turretPlaced = false;

    /* Hover */
    public bool mouseOverTurret;

    void Awake()
    {
        turret_colour_level = 1;
        turret_fireRate_level = 1;

        levelManager = GameObject.FindObjectOfType<LevelManager>(); //Saves LevelManager game object to a variable

        turret_name = this.turret_top.tag; //sets the turret name variable according to the top part's tag

        SetupTurretProperties(); //Sets up the turret properties
    }
    
    /* Firing at Enemy */
    private void Update()
    {
        if (!turretPlaced) //checks if the turret has been placed
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.turret_safeArea.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0f);
                turretPlaced = true;

                if (turret_name.Contains("single"))
                {
                    levelManager.money -= levelManager.cost_single;
                }
                else if (turret_name.Contains("dual"))
                {
                    levelManager.money -= levelManager.cost_dual;
                }
                else if (turret_name.Contains("triple"))
                {
                    levelManager.money -= levelManager.cost_triple;
                }
                else if (turret_name.Contains("sniper"))
                {
                    levelManager.money -= levelManager.cost_sniper;
                }
                else if (turret_name.Contains("missile"))
                {
                    levelManager.money -= levelManager.cost_missile;
                }
            }
            else
            {
                /* Follow Mouse */
                this.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
        else
        {
            FindClosestEnemy(); //Finds closest enemy

            /* Executes if an enemy has been found and the turret is ready to fire */
            if ((enemyFound) && ((Time.time - timeSinceLastShot) >= turret_fireRate))
            {
                /* Sets angle to rotate towards the enemy */
                Vector2 rotateTo = closestEnemy - new Vector2(transform.position.x, transform.position.y);
                float rotateToAngle = Mathf.Atan2(rotateTo.y, rotateTo.x) * Mathf.Rad2Deg;

                /* Rotates towards the enemy */
                turret_top.transform.rotation = Quaternion.AngleAxis(rotateToAngle, Vector3.forward);

                /* Saves the time of the last shot */
                timeSinceLastShot = Time.time;

                /* Executes if the turret is not a missile turret */
                if (turret_type != ("missile"))
                {
                    /* Executes for Green Turrets with a level 1 barrel */
                    if (turret_colour_level == 1)
                    {
                        audio_source.clip = bullet_lvl_1_shot;
                        audio_source.Play();

                        if (turret_type == ("single"))
                        {
                            Instantiate(bullet_lvl_1, this.single_barrel.transform.position, this.turret_top.transform.rotation);
                        }
                        else if (turret_type == ("dual"))
                        {
                            Instantiate(bullet_lvl_1, this.double_barrel_1.transform.position, this.turret_top.transform.rotation);
                            Instantiate(bullet_lvl_1, this.double_barrel_2.transform.position, this.turret_top.transform.rotation);
                        }
                        else if (turret_type == ("triple"))
                        {
                            Instantiate(bullet_lvl_1, this.triple_barrel_1.transform.position, this.turret_top.transform.rotation);
                            Instantiate(bullet_lvl_1, this.triple_barrel_2.transform.position, this.turret_top.transform.rotation);
                            Instantiate(bullet_lvl_1, this.triple_barrel_3.transform.position, this.turret_top.transform.rotation);
                        }
                        else if (turret_type == ("sniper"))
                        {
                            Instantiate(bullet_lvl_1, this.sniper_barrel.transform.position, this.turret_top.transform.rotation);
                        }
                    }
                    /* Executes for Blue Turrets with a level 2 barrel */
                    else if (turret_colour_level == 2)
                    {
                        audio_source.clip = bullet_lvl_2_shot;
                        audio_source.Play();

                        if (turret_name.Contains("single"))
                        {
                            Instantiate(bullet_lvl_2, this.single_barrel.transform.position, this.turret_top.transform.rotation);
                        }
                        else if (turret_name.Contains("dual"))
                        {
                            Instantiate(bullet_lvl_2, this.double_barrel_1.transform.position, this.turret_top.transform.rotation);
                            Instantiate(bullet_lvl_2, this.double_barrel_2.transform.position, this.turret_top.transform.rotation);
                        }
                        else if (turret_name.Contains("triple"))
                        {
                            Instantiate(bullet_lvl_2, this.triple_barrel_1.transform.position, this.turret_top.transform.rotation);
                            Instantiate(bullet_lvl_2, this.triple_barrel_2.transform.position, this.turret_top.transform.rotation);
                            Instantiate(bullet_lvl_2, this.triple_barrel_3.transform.position, this.turret_top.transform.rotation);
                        }
                        else if (turret_name.Contains("sniper"))
                        {
                            Instantiate(bullet_lvl_2, this.sniper_barrel.transform.position, this.turret_top.transform.rotation);
                        }
                    }
                    /* Executes for purple Turrets with a level 3 barrel */
                    else if (turret_colour_level == 3)
                    {
                        audio_source.clip = bullet_lvl_3_shot;
                        audio_source.Play();

                        int randomBullet;

                        if (turret_name.Contains("single"))
                        {
                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.single_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.single_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.single_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.single_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }
                        }
                        else if (turret_name.Contains("_dual_"))
                        {
                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.double_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.double_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.double_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.double_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }

                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.double_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.double_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.double_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.double_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }
                        }
                        else if (turret_name.Contains("_triple_"))
                        {
                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.triple_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.triple_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.triple_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.triple_barrel_1.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }

                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.triple_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.triple_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.triple_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.triple_barrel_2.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }

                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.triple_barrel_3.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.triple_barrel_3.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.triple_barrel_3.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.triple_barrel_3.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }
                        }
                        else if (turret_name.Contains("_sniper_"))
                        {
                            randomBullet = Random.Range(1, 5);

                            switch (randomBullet)
                            {
                                case 1:
                                    Instantiate(bullet_lvl_3_blue, this.sniper_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 2:
                                    Instantiate(bullet_lvl_3_green, this.sniper_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 3:
                                    Instantiate(bullet_lvl_3_purple, this.sniper_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                case 4:
                                    Instantiate(bullet_lvl_3_red, this.sniper_barrel.transform.position, this.turret_top.transform.rotation);
                                    break;
                                default:
                                    Debug.Log("Number Out of Range");
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    if (turret_colour_level == 1)
                    {
                        Instantiate(missile_lvl_1, this.missile_spot.transform.position, this.turret_top.transform.rotation);
                        this.turret_top.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                    }
                    else if (turret_colour_level == 2)
                    {
                        Instantiate(missile_lvl_2, this.missile_spot.transform.position, this.turret_top.transform.rotation);
                        this.turret_top.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                    }
                    else if (turret_colour_level == 3)
                    {
                        Instantiate(missile_lvl_3, this.missile_spot.transform.position, this.turret_top.transform.rotation);
                        this.turret_top.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                    }
                }
            }

            /* This code is for selectin/deselecting turrets */

            /* If this turret was clicked and the level manager allows the turret to be changed */
            if (clicked_thisTurret && levelManager.allowTurretTagChange)
            {
                /* Toggles to select / deselect turret if turret is clicked on */
                if (this.gameObject == levelManager.selectedTurret)
                {
                    this.tag = "Untagged";
                    clicked_thisTurret = false;
                }
                else
                {
                    this.gameObject.tag = "_selectedTurret";
                    clicked_thisTurret = false;
                }

                /* Checks if there is still a selected turret, if there isn't the upgrades canvas will be replaced with the last active canvas */
                if (levelManager.allTurrets_SelectedTag.Length > 0)
                {
                    levelManager.panel_upgrades.gameObject.SetActive(false);

                    if (levelManager.lastActiveCanvas) //turrets
                    {
                        levelManager.panel_turrets.gameObject.SetActive(true);
                    }
                    else if (!levelManager.lastActiveCanvas) //traps
                    {
                        levelManager.panel_traps.gameObject.SetActive(true);
                    }
                }
            }

            /* Checks if there is more than one turret with the _selectedTurret tag, then changes the old selected turret to its default tag */
            if (levelManager.allTurrets_SelectedTag.Length > 1)
            {
                if (this.gameObject == levelManager.selectedTurret)
                {
                    this.gameObject.tag = "Untagged";
                }
            }

            /* If this is the selected turret or the mouse is hovering on this turret, opacity will be lowered */
            if (mouseOverTurret || (this.gameObject.tag == "_selectedTurret"))
            {
                DecreaseOpacity();
            }
            else
            {
                IncreaseOpacity();
            }
        }
    }

    /* Selecting on Turret */
    private void OnMouseOver()
    {
        if ((Input.GetMouseButtonDown(0)) && (turretPlaced)) //checks if the left mouse button has was clicked and whether the turret has been placed
        {
            clicked_thisTurret = true;
        }
    }

    /* To check if mouse is hovered on or off the turret game object */
    private void OnMouseEnter()
    {
        if (turretPlaced) //checks if the turret has been placed
        {
            mouseOverTurret = true;
        }
    }
    private void OnMouseExit()
    {
        if (turretPlaced) //checks if the turret has been placed
        {
            mouseOverTurret = false;
        }
    }

    /* Decrease Opacity of Turret */
    private void DecreaseOpacity()
    {
        this.turret_top.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.7f);
        this.turret_base.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.7f);
        this.turret_safeArea.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
    }

    /* Increase Opacity of Turret */
    private void IncreaseOpacity()
    {
        this.turret_top.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1f);
        this.turret_base.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1f);
        this.turret_safeArea.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0f);
    }

    /* This function is used to find the closest enemy */
    void FindClosestEnemy() {
        enemyFound = false; //sets enemy found to false

        try
        {
            for (int h = 0; h < levelManager.hellhound.Length; h++) //repeats for the number of hellhounds
            {
                try
                {
                    /* Checks if the new hellhound in the array is closer to turret compared to the closest position stored in the closestEnemy variable */
                    if (Vector2.Distance(transform.position, levelManager.hellhound[h].transform.position) < Vector2.Distance(transform.position, new Vector2(closestEnemy.x, closestEnemy.y)))
                    {
                        try
                        {
                            closestEnemy = levelManager.hellhound[h].transform.position;
                            enemyFound = true;
                        }
                        catch
                        {
                            //enemy was destroyed or never was
                        }
                    }
                }
                catch
                {
                    //enemy was destroyed or never was
                }
            }
        }
        catch
        {
            //enemy was destroyed or never was
        }

        try
        {
            for (int z = 0; z < levelManager.zombie.Length; z++) //repeats for the number of zombies
            {
                try
                {   /* Checks if the new zombie in the array is closer to turret compared to the closest position stored in the closestEnemy variable */
                    if (Vector2.Distance(transform.position, levelManager.zombie[z].transform.position) < Vector2.Distance(transform.position, new Vector2(closestEnemy.x, closestEnemy.y)))
                    {
                        try
                        {
                            closestEnemy = levelManager.zombie[z].transform.position;
                            enemyFound = true;
                        }
                        catch
                        {
                            //enemy was destroyed or never was
                        }
                    }
                }
                catch
                {
                    //enemy was destroyed or never was
                }
            }
        }
        catch
        {
            //enemy was destroyed or never was
        }

        try
        {
            for (int b = 0; b < levelManager.boss.Length; b++) //repeats for the number of boss zombies
            {
                try
                {   /* Checks if the new boss zombie in the array is closer to turret compared to the closest position stored in the closestEnemy variable */
                    if ((Vector2.Distance(transform.position, levelManager.boss[b].transform.position) < Vector2.Distance(transform.position, new Vector2(closestEnemy.x, closestEnemy.y))))
                    {
                        try
                        {
                            closestEnemy = levelManager.boss[b].transform.position;
                            enemyFound = true;
                        }
                        catch
                        {
                            //enemy was destroyed or never was
                        }
                    }
                }
                catch
                {
                    //enemy was destroyed or never was
                }
            }
        }
        catch
        {
            //enemy was destroyed or never was
        }
    }

    /* Setting Up Turrets */
    void SetupTurretProperties()
    {
        /* Turret Preset Properties */

        /* Range */
        byte turret_single_range = 2;
        byte turret_dual_range = 3;
        byte turret_triple_range = 4;
        byte turret_sniper_range = 9;
        int turret_missile_range = 50000;

        /* Fire Rate */
        float turret_default_fireRate_Lv1 = 0.0700f;
        float turret_default_fireRate_Lv2 = 0.0350f;
        float turret_default_fireRate_Lv3 = 0.0175f;

        float turret_sniper_fireRate_Lv1 = 0.0700f;
        float turret_sniper_fireRate_Lv2 = 0.0350f;
        float turret_sniper_fireRate_Lv3 = 0.0175f;

        float turret_missile_fireRate_Lv1 = 0.0700f;
        float turret_missile_fireRate_Lv2 = 0.0350f;
        float turret_missile_fireRate_Lv3 = 0.0175f;

        /* Calculate Turret Type */
        //turret_type

        if (turret_name.Contains("_single_"))
        {
            turret_type = "single";
        }
        else if (turret_name.Contains("_dual_"))
        {
            turret_type = "dual";
        }
        else if (turret_name.Contains("_triple_"))
        {
            turret_type = "triple";
        }
        else if (turret_name.Contains("_sniper_"))
        {
            turret_type = "sniper";
        }
        else if (turret_name.Contains("_missile_"))
        {
            turret_type = "missile";
        }

        /* Calculate Turret colour Level */

        if (turret_colour_level == 1)
        {
            turret_colour = "green";
        }
        else if (turret_colour_level == 2)
        {
            turret_colour = "blue";
        }
        else if (turret_colour_level == 3)
        {
            turret_colour = "purple";
        }

        /* Calculate Fire Rate */

        if ((turret_name.Contains("_single_")) || (turret_name.Contains("_dual_")) || (turret_name.Contains("_triple_")))
        {
            if (turret_fireRate_level == 1)
            {
                turret_fireRate = turret_default_fireRate_Lv1;
            }
            else if (turret_fireRate_level == 2)
            {
                turret_fireRate = turret_default_fireRate_Lv2;
            }
            else if (turret_fireRate_level == 3)
            {
                turret_fireRate = turret_default_fireRate_Lv3;
            }
        }
        else if (turret_name.Contains("_sniper_"))
        {
            if (turret_fireRate_level == 1)
            {
                turret_fireRate = turret_sniper_fireRate_Lv1;
            }
            else if (turret_fireRate_level == 2)
            {
                turret_fireRate = turret_sniper_fireRate_Lv2;
            }
            else if (turret_fireRate_level == 3)
            {
                turret_fireRate = turret_sniper_fireRate_Lv3;
            }
        }
        else if (turret_name.Contains("_missile_"))
        {
            if (turret_fireRate_level == 1)
            {
                turret_fireRate = turret_missile_fireRate_Lv1;
            }
            else if (turret_fireRate_level == 2)
            {
                turret_fireRate = turret_missile_fireRate_Lv2;
            }
            else if (turret_fireRate_level == 3)
            {
                turret_fireRate = turret_missile_fireRate_Lv3;
            }
        }

        /* Calculate Firing Range */

        /* Range */
        if (turret_name.Contains("_single_"))
        {
            turret_range = turret_single_range;
        }
        else if (turret_name.Contains("_dual_"))
        {
            turret_range = turret_dual_range;
        }
        else if (turret_name.Contains("_triple_"))
        {
            turret_range = turret_triple_range;
        }
        else if (turret_name.Contains("_sniper_"))
        {
            turret_range = turret_sniper_range;
        }
        else if (turret_name.Contains("_missile_"))
        {
            turret_range = turret_missile_range;
        }

        this.gameObject.name = ("turret_" + turret_colour + "_" + turret_type + "_lv" + turret_fireRate_level);

        if (turret_type != "missile")
        {
            string path_turret_top = "Images/Turrets/" + turret_colour_level + "_" + turret_colour + "/" + turret_type + "/turret_" + turret_colour + "_" + turret_type + "_top_lv" + turret_fireRate_level;
            this.turret_top.GetComponent<SpriteRenderer>().sprite = Resources.Load(path_turret_top, typeof(Sprite)) as Sprite;
            Debug.Log(path_turret_top);
        }
        else
        {
            string path_turret_top = "Images/Missiles/" + turret_type + "_lv" + turret_fireRate_level;
            this.turret_top.GetComponent<SpriteRenderer>().sprite = Resources.Load(path_turret_top, typeof(Sprite)) as Sprite;
            Debug.Log(path_turret_top);
        }
        
        if ((turret_type == "single") || (turret_type == "dual") || (turret_type == "triple"))
        {
            string path_turret_base = "Images/Turrets/" + turret_colour_level + "_" + turret_colour + "/bottom/turret_" + turret_colour + "_single-dual-triple_bottom";
            this.turret_base.GetComponent<SpriteRenderer>().sprite = Resources.Load(path_turret_base, typeof(Sprite)) as Sprite;
            Debug.Log(path_turret_base);
        }
        else
        {
            string path_turret_base = "Images/Turrets/" + turret_colour_level + "_" + turret_colour + "/bottom/turret_" + turret_colour + "_" + turret_type + "_bottom";
            this.turret_base.GetComponent<SpriteRenderer>().sprite = Resources.Load(path_turret_base, typeof(Sprite)) as Sprite;
            Debug.Log(path_turret_base);
        }
    }

    /* Upgrading Turrets */
    public void UpgradeTurret_colour()
    {
        turret_colour_level++;
        SetupTurretProperties();
    }

    public void UpgradeTurret_fireRate()
    {
        turret_fireRate_level++;
        SetupTurretProperties();
    }

}

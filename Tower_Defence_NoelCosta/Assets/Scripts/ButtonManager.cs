﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Linq;


public class ButtonManager : MonoBehaviour
{
    public LevelManager levelManager;
    public MainMenu menuManager;

    public GameObject turret_single;
    public GameObject turret_double;
    public GameObject turret_triple;
    public GameObject turret_sniper;
    public GameObject turret_missile;

    public string hs_1_lvl_garden = null;
    public string hs_2_lvl_garden = null;
    public string hs_3_lvl_garden = null;

    public string hs_1_lvl_desert = null;
    public string hs_2_lvl_desert = null;
    public string hs_3_lvl_desert = null;

    public string hs_1_lvl_mars = null;
    public string hs_2_lvl_mars = null;
    public string hs_3_lvl_mars = null;
    

    private void Awake()
    {
        try
        {
            levelManager = GameObject.FindObjectOfType<LevelManager>(); //Saves LevelManager game object to a variable
        }
        catch
        {
            //no level manager
        }

        try
        {
            menuManager = GameObject.FindObjectOfType<MainMenu>(); //Saves MainMenu game object to a variable
        }
        catch
        {
            //currently in a level - not the main menu
        }
    }

    public void btn_click_switch_turrets()
    {
        levelManager.lastActiveCanvas = false;

        levelManager.panel_turrets.gameObject.SetActive(false);
        levelManager.panel_traps.gameObject.SetActive(true);
    }

    /* During Gameplay */
    public void btn_click_turret_single()
    {
        if (levelManager.money - levelManager.cost_single >= 0)
        {
            GameObject prefab_turret_single = Resources.Load("Prefabs/Turrets/single/turret_green_single", typeof(GameObject)) as GameObject;
            Instantiate(prefab_turret_single, Camera.main.ScreenToWorldPoint(Input.mousePosition), this.transform.rotation);
        }
    }

    public void btn_click_turret_dual()
    {
        if (levelManager.money - levelManager.cost_dual >= 0)
        {
            GameObject prefab_turret_dual = Resources.Load("Prefabs/Turrets/dual/turret_green_dual", typeof(GameObject)) as GameObject;
            Instantiate(prefab_turret_dual, Camera.main.ScreenToWorldPoint(Input.mousePosition), this.transform.rotation);
        }
    }

    public void btn_click_turret_triple()
    {
        if (levelManager.money - levelManager.cost_triple >= 0)
        {
            GameObject prefab_turret_triple = Resources.Load("Prefabs/Turrets/triple/turret_green_triple", typeof(GameObject)) as GameObject;
            Instantiate(prefab_turret_triple, Camera.main.ScreenToWorldPoint(Input.mousePosition), this.transform.rotation);
        }
    }

    public void btn_click_turret_sniper()
    {
        if (levelManager.money - levelManager.cost_sniper >= 0)
        {
            GameObject prefab_turret_sniper = Resources.Load("Prefabs/Turrets/sniper/turret_green_sniper", typeof(GameObject)) as GameObject;
            Instantiate(prefab_turret_sniper, Camera.main.ScreenToWorldPoint(Input.mousePosition), this.transform.rotation);
        }
    }

    public void btn_click_turret_missile()
    {
        if (levelManager.money - levelManager.cost_missile >= 0)
        {
            GameObject prefab_turret_missile = Resources.Load("Prefabs/Turrets/missile/turret_green_missile", typeof(GameObject)) as GameObject;
            Instantiate(prefab_turret_missile, Camera.main.ScreenToWorldPoint(Input.mousePosition), this.transform.rotation);
        }
    }

    /* Traps */
    public void btn_click_switch_traps()
    {
        levelManager.lastActiveCanvas = true;

        levelManager.panel_turrets.gameObject.SetActive(true);
        levelManager.panel_traps.gameObject.SetActive(false);
    }

    public void btn_click_trap_acid()
    {


        //Code Here
    }

    public void btn_click_trap_spikes()
    {


        //Code Here
    }

    /* Upgrades */
    public void btn_click_close_upgrades()
    {
        deselectTurret();

        levelManager.panel_upgrades.gameObject.SetActive(false);

        if (levelManager.lastActiveCanvas) //turrets
        {
            levelManager.panel_turrets.gameObject.SetActive(true);
        }
        else if (!levelManager.lastActiveCanvas) //traps
        {
            levelManager.panel_traps.gameObject.SetActive(true);
        }
    }

    public void btn_click_upgrade_level()
    {
        Turrets selectedTurret_script = levelManager.selectedTurret.GetComponent(typeof(Turrets)) as Turrets;

        if ((levelManager.money - levelManager.cost_upgrades >= 0) && (selectedTurret_script.turret_colour_level <= 2))
        {
            levelManager.money -= levelManager.cost_upgrades;


            selectedTurret_script.UpgradeTurret_colour();
        }
    }

    public void btn_click_upgrade_fireRate()
    {
        Turrets selectedTurret_script = levelManager.selectedTurret.GetComponent(typeof(Turrets)) as Turrets;

        if ((levelManager.money - levelManager.cost_upgrades >= 0) && (selectedTurret_script.turret_fireRate_level <= 2))
        {
            levelManager.money -= levelManager.cost_upgrades;

            selectedTurret_script.UpgradeTurret_fireRate();
        }
    }

    /* Deselect selected turret */
    public void deselectTurret()
    {
        try
        {
            GameObject selectedTurret = levelManager.selectedTurret;

            levelManager.selectedTurret.tag = "Untagged";
        }
        catch
        {
            //There are no selected turrets
        }
    }

    /* Menu - Main */
    public void btn_click_main_play()
    {
        menuManager.panel_menu_main.gameObject.SetActive(false);
        menuManager.panel_menu_play.gameObject.SetActive(true);
    }

    public void btn_click_main_leaderboards()
    {
        menuManager.panel_menu_main.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards.gameObject.SetActive(true);
    }

    public void btn_click_main_quit()
    {
        Application.Quit();
    }

    /* Menu - Play */
    public void btn_click_play_lvl_garden()
    {
        SceneManager.LoadScene("lvl_garden");
    }

    public void btn_click_play_lvl_desert()
    {
        SceneManager.LoadScene("lvl_desert");
    }

    public void btn_click_play_lvl_mars()
    {
        SceneManager.LoadScene("lvl_mars");
    }

    public void btn_click_play_back()
    {
        menuManager.panel_menu_play.gameObject.SetActive(false);
        menuManager.panel_menu_main.gameObject.SetActive(true);
    }

    /* Menu Leaderboards - Main */
    public void btn_click_main_leaderboards_back()
    {
        menuManager.panel_menu_leaderboards.gameObject.SetActive(false);
        menuManager.panel_menu_main.gameObject.SetActive(true);
    }

    /* Menu - Leaderboards - Garden */
    public void btn_click_leaderboards_garden()
    {
        seperateText();

        menuManager.panel_menu_leaderboards.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards_garden.gameObject.SetActive(true);
    }

    public void btn_click_leaderboards_garden_back()
    {
        menuManager.panel_menu_leaderboards_garden.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards.gameObject.SetActive(true);
    }

    /* Menu - Leaderboards - Desert */
    public void btn_click_leaderboards_desert()
    {
        seperateText();

        menuManager.panel_menu_leaderboards.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards_desert.gameObject.SetActive(true);
    }

    public void btn_click_leaderboards_desert_back()
    {
        menuManager.panel_menu_leaderboards_desert.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards.gameObject.SetActive(true);
    }

    /* Menu - Leaderboards - mars */
    public void btn_click_leaderboards_mars()
    {
        seperateText();

        menuManager.panel_menu_leaderboards.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards_mars.gameObject.SetActive(true);
    }

    public void btn_click_leaderboards_mars_back()
    {
        menuManager.panel_menu_leaderboards_mars.gameObject.SetActive(false);
        menuManager.panel_menu_leaderboards.gameObject.SetActive(true);
    }

    /* Text File Methods */
    public void seperateText()
    {
        try
        {
            //TextAsset learderboardsTextAsset = Resources.Load("textFiles/leaderboards.txt") as TextAsset;

            //List<string> allLeaderboardEntries = learderboardsTextAsset.text.Split('~').ToList();
            //List<string> allLeaderboardEntries = new("d");

            string[] allLeaderboardEntries = File.ReadAllLines(Application.dataPath + "/textFiles/leaderboards.txt");

            //List<string> allLeaderboardEntries = new List<string>(result.Split('~').ToList());

            List<string> level = new List<string>();
            List<string> name = new List<string>();
            List<string> round = new List<string>();
            List<string> kills = new List<string>();
            List<int> score = new List<int>();

            for (int i = 0; i < allLeaderboardEntries.Length; i++)
            {
                List<string> seperate = allLeaderboardEntries[i].Split(',').ToList();

                level[i] = seperate[1];
                name[i] = seperate[2];
                round[i] = seperate[3];
                kills[i] = seperate[4];
                score[i] = int.Parse(seperate[5]);
            }

            int hs_1_lvl_garden_temp = 0;
            int hs_2_lvl_garden_temp = 0;
            int hs_3_lvl_garden_temp = 0;

            int hs_1_lvl_desert_temp = 0;
            int hs_2_lvl_desert_temp = 0;
            int hs_3_lvl_desert_temp = 0;

            int hs_1_lvl_mars_temp = 0;
            int hs_2_lvl_mars_temp = 0;
            int hs_3_lvl_mars_temp = 0;

            for (int i = 0; i < level.Count; i++)
            {
                if (level[i] == "lvl_garden")
                {
                    if (score[i] <= hs_1_lvl_garden_temp)
                    {
                        hs_3_lvl_garden_temp = hs_2_lvl_garden_temp;
                        hs_2_lvl_garden_temp = hs_1_lvl_garden_temp;
                        hs_1_lvl_garden_temp = i;
                    }
                    else if((score[i] <= hs_2_lvl_garden_temp))
                    {
                        hs_3_lvl_garden_temp = hs_2_lvl_garden_temp;
                        hs_2_lvl_garden_temp = i;
                    }
                    else if ((score[i] <= hs_3_lvl_garden_temp))
                    {
                        hs_3_lvl_garden_temp = i;
                    }
                }

                if (level[i] == "lvl_desert")
                {
                    if (score[i] <= hs_1_lvl_desert_temp)
                    {
                        hs_3_lvl_desert_temp = hs_2_lvl_desert_temp;
                        hs_2_lvl_desert_temp = hs_1_lvl_desert_temp;
                        hs_1_lvl_desert_temp = i;
                    }
                    else if ((score[i] <= hs_2_lvl_desert_temp))
                    {
                        hs_3_lvl_desert_temp = hs_2_lvl_desert_temp;
                        hs_2_lvl_desert_temp = i;
                    }
                    else if ((score[i] <= hs_3_lvl_desert_temp))
                    {
                        hs_3_lvl_desert_temp = i;
                    }
                }

                if (level[i] == "lvl_mars")
                {
                    if (score[i] <= hs_1_lvl_mars_temp)
                    {
                        hs_3_lvl_mars_temp = hs_2_lvl_mars_temp;
                        hs_2_lvl_mars_temp = hs_1_lvl_mars_temp;
                        hs_1_lvl_mars_temp = i;
                    }
                    else if ((score[i] <= hs_2_lvl_mars_temp))
                    {
                        hs_3_lvl_mars_temp = hs_2_lvl_mars_temp;
                        hs_2_lvl_mars_temp = i;
                    }
                    else if ((score[i] <= hs_3_lvl_mars_temp))
                    {
                        hs_3_lvl_mars_temp = i;
                    }
                }
            }

            //hs_1_lvl_garden =  hs_1_lvl_garden_temp;

            menuManager.leaderboards_garden_1.text = "| 01 | lvl_garden | " + name[hs_1_lvl_garden_temp] + " | " + round[hs_1_lvl_garden_temp] + " | " + kills[hs_1_lvl_garden_temp] + " | " + score[hs_1_lvl_garden_temp] + " |";
            menuManager.leaderboards_garden_2.text = "| 02 | lvl_garden | " + name[hs_2_lvl_garden_temp] + " | " + round[hs_2_lvl_garden_temp] + " | " + kills[hs_2_lvl_garden_temp] + " | " + score[hs_2_lvl_garden_temp] + " |";
            menuManager.leaderboards_garden_3.text = "| 03 | lvl_garden | " + name[hs_3_lvl_garden_temp] + " | " + round[hs_3_lvl_garden_temp] + " | " + kills[hs_3_lvl_garden_temp] + " | " + score[hs_3_lvl_garden_temp] + " |";

            menuManager.leaderboards_desert_1.text = "| 01 | lvl_desert | " + name[hs_1_lvl_desert_temp] + " | " + round[hs_1_lvl_desert_temp] + " | " + kills[hs_1_lvl_desert_temp] + " | " + score[hs_1_lvl_desert_temp] + " |";
            menuManager.leaderboards_desert_2.text = "| 02 | lvl_desert | " + name[hs_2_lvl_desert_temp] + " | " + round[hs_2_lvl_desert_temp] + " | " + kills[hs_2_lvl_desert_temp] + " | " + score[hs_2_lvl_desert_temp] + " |";
            menuManager.leaderboards_desert_3.text = "| 03 | lvl_desert | " + name[hs_3_lvl_desert_temp] + " | " + round[hs_3_lvl_desert_temp] + " | " + kills[hs_3_lvl_desert_temp] + " | " + score[hs_3_lvl_desert_temp] + " |";

            menuManager.leaderboards_mars_1.text = "| 01 | lvl_mars | " + name[hs_1_lvl_mars_temp] + " | " + round[hs_1_lvl_mars_temp] + " | " + kills[hs_1_lvl_mars_temp] + " | " + score[hs_1_lvl_mars_temp] + " |";
            menuManager.leaderboards_mars_2.text = "| 02 | lvl_mars | " + name[hs_2_lvl_mars_temp] + " | " + round[hs_2_lvl_mars_temp] + " | " + kills[hs_2_lvl_mars_temp] + " | " + score[hs_2_lvl_mars_temp] + " |";
            menuManager.leaderboards_mars_3.text = "| 03 | lvl_mars | " + name[hs_3_lvl_mars_temp] + " | " + round[hs_3_lvl_mars_temp] + " | " + kills[hs_3_lvl_mars_temp] + " | " + score[hs_3_lvl_mars_temp] + " |";

        }
        catch
        {
            if (!Directory.Exists(Application.dataPath + "/textFiles/leaderboards.txt"))
            {
                StreamWriter outStream = new StreamWriter(Application.dataPath + "/textFiles/leaderboards.txt");
                Debug.Log("Leaderboards Text File Created");
            }
            else
            {
                Debug.Log("Text File Empty");
            }
            
            menuManager.leaderboards_garden_1.text = "- Empty -";
            menuManager.leaderboards_garden_2.text = "- Empty -";
            menuManager.leaderboards_garden_3.text = "- Empty -";

            menuManager.leaderboards_desert_1.text = "- Empty -";
            menuManager.leaderboards_desert_2.text = "- Empty -";
            menuManager.leaderboards_desert_3.text = "- Empty -";

            menuManager.leaderboards_mars_1.text = "- Empty -";
            menuManager.leaderboards_mars_2.text = "- Empty -";
            menuManager.leaderboards_mars_3.text = "- Empty -";
        }

    }
}

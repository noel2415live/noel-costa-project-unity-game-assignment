﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    /* Public variables for drawing dots */
    public List<Vector2> allPointsList = new List<Vector2>(); //Every point will be stored in here, so it can be used by the enemy script
    [SerializeField] private List<Transform> pathRoute_1 = new List<Transform>(); //The points placed will be stored here
    [SerializeField] private float spacing = 0.1f; //spacing between each generated dot

    /* Public arrays for enemy type on scene */
    public GameObject[] hellhound;
    public GameObject[] zombie;
    public GameObject[] boss;

    /* Public gameobject & array to identify the selected turret */
    public GameObject selectedTurret;
    public GameObject[] allTurrets_SelectedTag;

    /* This boolean allows the turret tag to change */
    public bool allowTurretTagChange;

    /* Player Stats */
    public int baseHealth;
    public int money;
    public int kills;
    public int roundNumber;
    public int score;

    /* HUD Text Fields */
    public Text text_health;
    public Text text_roundNumber;
    public Text text_money;

    public Canvas panel_turrets;
    public Canvas panel_traps;
    public Canvas panel_upgrades;

    /* Canvas */
    public bool lastActiveCanvas; //true -> turrets / false -> traps

    /* Turret Costs */
    public int cost_single = 500;
    public int cost_dual = 1500;
    public int cost_triple = 4500;
    public int cost_sniper = 1000;
    public int cost_missile = 7000;

    /* Traps Costs */
    public int cost_acid = 1500;
    public int cost_spikes = 1000;

    /* Upgrade Costs */
    public int cost_upgrades = 2500;

    /* Buying turret variables */
    public bool isBuying;
    public byte buying_itemNumber;

    /* Zombies Spawning */
    public bool roundEnded;
    public bool zombiesThisRound;
    //public bool ;

    private void Awake()
    {
        money = 70000;
        baseHealth = 150;

        text_health = GameObject.Find("text_health").GetComponent<Text>();
        text_roundNumber = GameObject.Find("text_roundNumber").GetComponent<Text>();
        text_money = GameObject.Find("text_money").GetComponent<Text>();

        panel_turrets = GameObject.Find("Buttons_Turrets").GetComponent<Canvas>();
        panel_traps = GameObject.Find("Buttons_Traps").GetComponent<Canvas>();
        panel_upgrades = GameObject.Find("Buttons_Upgrades").GetComponent<Canvas>();

        lastActiveCanvas = true; //Set to turret canvas

        panel_traps.gameObject.SetActive(false);
        panel_upgrades.gameObject.SetActive(false);

        roundNumber++;
    }

    private void FixedUpdate()
    {
        /* Finds enemy type and places them in the corresponding array */
        hellhound = GameObject.FindGameObjectsWithTag("enemy_hellhound");
        zombie = GameObject.FindGameObjectsWithTag("enemy_zombie");
        boss = GameObject.FindGameObjectsWithTag("enemy_boss");

        /* Updates Text on HUD */
        text_health.text = baseHealth.ToString();
        text_money.text = money.ToString();
        text_roundNumber.text = "Round: " + roundNumber;
    }

    private void Update()
    {
        try
        {
            /* Searches for game objects with the _selectedTurret tag */
            allTurrets_SelectedTag = GameObject.FindGameObjectsWithTag("_selectedTurret");
        }
        catch
        {
            //no turret selected
        }
        /* Makes sure that the selected turret array and gameobject correspond with each other */
        if (allTurrets_SelectedTag.Length == 0)
        {
            selectedTurret = null;
        }
        else
        {
            selectedTurret = allTurrets_SelectedTag[0];
        }

        allowTurretTagChange = true; //sets the turret tag to beable to change

        if (selectedTurret)
        {
            panel_turrets.gameObject.SetActive(false);
            panel_traps.gameObject.SetActive(false);
            panel_upgrades.gameObject.SetActive(true);
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }

    }

    /* Generate / Draw Path on Scene Editor */
    private void OnDrawGizmos()
    {
        allPointsList.Clear(); //Clears the list

        /* The user placed points will be copied to a Vector 2 List */
        for (int i = 0; i < pathRoute_1.Count; i++)
        {
            allPointsList.Add(pathRoute_1[i].position);
        }

        calcEvenSpacedPoints(); //Calls the function to generate the evenly spaced points

    }

    /* The function below will generate points between the placed points in the Unity Editor */
    public void calcEvenSpacedPoints()
    {
        if (spacing > 0) { //Makes sure it doesn't run out of memory if mistakenly set to 0
            /* This list will hold every generated point */
            List<Vector2> evenSpacedPointslist = new List<Vector2>();
            evenSpacedPointslist.Add(allPointsList[0]);

            /* This variable will hold the previous code for generation */
            Vector2 previousPoint = allPointsList[0];
            float distFromLastEvenPoint = 0;

            /* This for loop will generate the points between the ones placed in the Unity editor */
            for (int i = 0; i < (allPointsList.Count / 3); i++)
            {
                Vector2[] points = new Vector2[] { allPointsList[i * 3], allPointsList[i * 3 + 1], allPointsList[i * 3 + 2], allPointsList[((i * 3 + 3) + allPointsList.Count) % allPointsList.Count] };

                float time = 0;
                while (time <= 1)
                {
                    time += 0.1f;

                    Vector2 pointOnCurve = cubicCurve(points[0], points[1], points[2], points[3], time);
                    distFromLastEvenPoint += Vector2.Distance(previousPoint, pointOnCurve); //Calculated the distance from the last even point

                    /* This while loop prevents the distance of each generated point to be too far */
                    while (distFromLastEvenPoint >= spacing)
                    {

                        float overshotDistance = distFromLastEvenPoint - spacing;
                        Vector2 newEvenSpacedPoint = pointOnCurve + (previousPoint - pointOnCurve).normalized * overshotDistance;
                        evenSpacedPointslist.Add(newEvenSpacedPoint); //Evenly generated points are then added to the evenSpacedPointslist list

                        distFromLastEvenPoint = overshotDistance;

                        previousPoint = newEvenSpacedPoint; //sets the previous point to the newly calculated point
                    }

                    previousPoint = pointOnCurve; //Sets the previous point to point on the curve calculated before the while loop
                }
            }

            /* The allPointsList will be cleared and will be filled up by every generated point */
            allPointsList.Clear();

            for (int i = 0; i < evenSpacedPointslist.Count; i++)
            {
                /* Gizmos are used to visualize points on the Unity Editor, in this case in the form of spheres */
                Gizmos.DrawSphere(evenSpacedPointslist[i], 0.1f); //Second value is the size

                /* The location of these points is also added to the allPointsList list*/
                allPointsList.Add(evenSpacedPointslist[i]);

            }
        }
    }

    /* The following 3 functions are used for calculating both linear and curved paths */
    /* Two Points - linear */
    public static Vector2 linearPath(Vector2 point_1, Vector2 point_2, float time) {
        return point_1 + (point_2 - point_1) * time;
    }

    /* Three Points - curved */
    public static Vector2 quadraticCurve(Vector2 point_1, Vector2 point_2, Vector2 point_3, float time) {
        Vector2 curvePoint_1 = linearPath(point_1, point_2, time);
        Vector2 curvePoint_2 = linearPath(point_2, point_3, time);

        return linearPath(curvePoint_1, curvePoint_2, time);
    }

    /* Four Points - curved */
    public static Vector2 cubicCurve(Vector2 point_1, Vector2 point_2, Vector2 point_3, Vector2 point_4, float time)
    {
        Vector2 curvePoint_1 = quadraticCurve(point_1, point_2, point_3, time);
        Vector2 curvePoint_2 = quadraticCurve(point_2, point_3, point_4, time);

        return linearPath(curvePoint_1, curvePoint_2, time);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Canvas panel_menu_play;
    public Canvas panel_menu_main;
    public Canvas panel_menu_leaderboards;
    public Canvas panel_menu_leaderboards_garden;
    public Canvas panel_menu_leaderboards_desert;
    public Canvas panel_menu_leaderboards_mars;

    public Text leaderboards_garden_1;
    public Text leaderboards_garden_2;
    public Text leaderboards_garden_3;

    public Text leaderboards_desert_1;
    public Text leaderboards_desert_2;
    public Text leaderboards_desert_3;

    public Text leaderboards_mars_1;
    public Text leaderboards_mars_2;
    public Text leaderboards_mars_3;
    
    // Start is called before the first frame update
    void Awake()
    {
        panel_menu_play = GameObject.Find("canvas_menu_play").GetComponent<Canvas>();
        panel_menu_main = GameObject.Find("canvas_menu_main").GetComponent<Canvas>();
        panel_menu_leaderboards = GameObject.Find("canvas_menu_leaderboards").GetComponent<Canvas>();
        panel_menu_leaderboards_garden = GameObject.Find("canvas_menu_leaderboards_garden").GetComponent<Canvas>();
        panel_menu_leaderboards_desert = GameObject.Find("canvas_menu_leaderboards_desert").GetComponent<Canvas>();
        panel_menu_leaderboards_mars = GameObject.Find("canvas_menu_leaderboards_mars").GetComponent<Canvas>();

        panel_menu_main.gameObject.SetActive(true);
        panel_menu_play.gameObject.SetActive(false);
        panel_menu_leaderboards.gameObject.SetActive(false);
        panel_menu_leaderboards_garden.gameObject.SetActive(false);
        panel_menu_leaderboards_desert.gameObject.SetActive(false);
        panel_menu_leaderboards_mars.gameObject.SetActive(false);

        leaderboards_garden_1 = GameObject.Find("txt_lead_garden_1").GetComponent<Text>();
        leaderboards_garden_2 = GameObject.Find("txt_lead_garden_2").GetComponent<Text>();
        leaderboards_garden_3 = GameObject.Find("txt_lead_garden_3").GetComponent<Text>();

        leaderboards_desert_1 = GameObject.Find("txt_lead_desert_1").GetComponent<Text>();
        leaderboards_desert_2 = GameObject.Find("txt_lead_desert_2").GetComponent<Text>();
        leaderboards_desert_3 = GameObject.Find("txt_lead_desert_3").GetComponent<Text>();

        leaderboards_mars_1 = GameObject.Find("txt_lead_mars_1").GetComponent<Text>();
        leaderboards_mars_2 = GameObject.Find("txt_lead_mars_2").GetComponent<Text>();
        leaderboards_mars_3 = GameObject.Find("txt_lead_mars_3").GetComponent<Text>();
    }

    /* Menu - Main */
    public void btn_click_main_play()
    {
        Canvas panel_menu_main = GameObject.Find("canvas_menu_main").GetComponent<Canvas>();
        Canvas panel_menu_play = GameObject.Find("canvas_menu_play").GetComponent<Canvas>();

        panel_menu_main.gameObject.SetActive(false);
        panel_menu_play.gameObject.SetActive(true);
    }
}
